msgid ""
msgstr ""
"Project-Id-Version: harden-doc 3.19\n"
"POT-Creation-Date: 2017-05-01 19:29+0200\n"
"PO-Revision-Date: 2017-04-24 01:07+0200\n"
"Last-Translator: desconocido <desconocido@unknow.com>\n"
"Language-Team: debian-l10n-spanish@lists.debian.org\n"
"Language: es \n"
"Report-Msgid-Bugs-To: https://www.debian.org/Bugs/\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Publican v4.3.2\n"

msgid "Introduction"
msgstr "Introducción"

msgid "One of the hardest things about writing security documents is that every case is unique. Two things you have to pay attention to are the threat environment and the security needs of the individual site, host, or network. For instance, the security needs of a home user are completely different from a network in a bank. While the primary threat a home user needs to face is the script kiddie type of cracker, a bank network has to worry about directed attacks. Additionally, the bank has to protect their customer's data with arithmetic precision. In short, every user has to consider the trade-off between usability and security/paranoia."
msgstr "Una de las cosas más dificiles sobre los documentos de seguridad es que cada caso es único. Dos cosas a las que se debe prestar atención son la amenaza del entorno y las necesidades de seguridad, tanto de cada parte individual como del servidor o de la red. Por ejemplo, las necesidades de seguridad de un usuario local son completamente diferentes a las de la red de un banco. Mientras que un usuario local necesita defenderse contra el cracker <emphasis>script-kiddie</emphasis>, un banco tiene que preocuparse de ataques dirigidos. Además, el banco tiene que proteger los datos de sus clientes con precisión milimétrica. En resumen, todo usuario debe considerar el equilibrio entre utilización y seguridad/paranoia."

msgid "Note that this manual only covers issues relating to software. The best software in the world can't protect you if someone can physically access the machine. You can place it under your desk, or you can place it in a hardened bunker with an army in front of it. Nevertheless the desktop computer can be much more secure (from a software point of view) than a physically protected one if the desktop is configured properly and the software on the protected machine is full of security holes. Obviously, you must consider both issues."
msgstr "Observe que este manual solamente trata de asuntos relacionados con el software. Ni el mejor software del mundo podría protegerlo si alguien tuviera acceso físico a la máquina. Usted puede colocarla bajo su mesa o puede ponerla en un búnker con un ejército que la protega. Sin embargo, un ordenador de escritorio puede ser muchísimo más seguro (desde el punto de vista del software) que un sistema protegido físicamente si el primero de ellos se configura de la manera apropiada y el segundo está lleno de agujeros de seguridad. Lógicamente, usted debe considerar ambos casos."

msgid "This document just gives an overview of what you can do to increase the security of your Debian GNU/Linux system. If you have read other documents regarding Linux security, you will find that there are common issues which might overlap with this document. However, this document does not try to be the ultimate source of information you will be using, it only tries to adapt this same information so that it is meaningful to a Debian GNU/Linux system. Different distributions do some things in different ways (startup of daemons is one example); here, you will find material which is appropriate for Debian's procedures and tools."
msgstr "Este documento da una apreciación global de lo que usted puede hacer para incrementar la seguridad de su sistema Debian GNU/Linux. Si usted ha leido otros documentos con respecto a la seguridad en Linux, encontrará que describen problemas comunes, los cuales pueden solaparse con este documento. Sin embargo este documento no intenta ser la única fuente de información que usted debería usar, sólo intenta adaptar esa misma información para su aplicación sobre un sistema Debian GNU/Linux. La forma de trabajar de distintas distribuciones es diferente (el ejemplo habitual es la forma de arrancar y para los demonios del sistema); aquí usted encontrará material apropiado para los procedimientos y herramientas utilizadas por Debian."

msgid "Authors"
msgstr ""

msgid "The current maintainer of this document is Javier Fernández-Sanguino Peña. Please forward him any comments, additions or suggestions, and they will be considered for inclusion in future releases of this manual."
msgstr ""

msgid "This manual was started as a <emphasis>HOWTO</emphasis> by Alexander Reelsen. After it was published on the Internet, Javier Fernández-Sanguino Peña incorporated it into the <ulink url=\"http://www.debian.org/doc\">Debian Documentation Project</ulink>. A number of people have contributed to this manual (all contributions are listed in the changelog) but the following deserve special mention since they have provided significant contributions (full sections, chapters or appendices):"
msgstr ""

msgid "Stefano Canepa"
msgstr "Stefano Canepa"

msgid "Era Eriksson"
msgstr "Era Eriksson"

msgid "Carlo Perassi"
msgstr "Carlo Perassi"

msgid "Alexandre Ratti"
msgstr "Alexandre Ratti"

msgid "Jaime Robles"
msgstr "Jaime Robles"

msgid "Yotam Rubin"
msgstr "Yotam Rubin"

msgid "Frederic Schutz"
msgstr "Frederic Schutz"

msgid "Pedro Zorzenon Neto"
msgstr "Pedro Zorzenon Neto"

msgid "Oohara Yuuma"
msgstr "Oohara Yuuma"

msgid "Davor Ocelic"
msgstr "Davor Ocelic"

msgid "Where to get the manual (and available formats)"
msgstr "Lugares donde encontrar este manual (diversos formatos)"

msgid "You can download or view the latest version of the Securing Debian Manual from the <ulink url=\"http://www.debian.org/doc/manuals/securing-debian-howto/\">Debian Documentation Project</ulink>. If you are reading a copy from another site, please check the primary copy in case it provides new information. If you are reading a translation, please review the version the translation refers to to the latest version available. If you find that the version is behind please consider using the original copy or review the to see what has changed."
msgstr ""

msgid "If you want a full copy of the manual you can either download the <ulink url=\"http://www.debian.org/doc/manuals/securing-debian-howto/securing-debian-howto.en.txt\">text version</ulink> or the <ulink url=\"http://www.debian.org/doc/manuals/securing-debian-howto/securing-debian-howto.en.pdf\">PDF version</ulink> from the Debian Documentation Project's site. These versions might be more useful if you intend to copy the document over to a portable device for offline reading or you want to print it out. Be forewarned, the manual is over two hundred pages long and some of the code fragments, due to the formatting tools used, are not wrapped in the PDF version and might be printed incomplete."
msgstr ""

msgid "The document is also provided in text, html and PDF formats in the <ulink url=\"http://packages.debian.org/harden-doc\">harden-doc</ulink> package. Notice, however, that the package maybe not be completely up to date with the document provided on the Debian site (but you can always use the source package to build an updated version yourself)."
msgstr ""

msgid "This document is part of the documents distributed by the <ulink url=\"https://alioth.debian.org/projects/ddp/\">Debian Documentation Project</ulink>. You can review the changes introduced in the document using a web browser and obtaining information from the <ulink url=\"http://anonscm.debian.org/viewvc/ddp/manuals/trunk/securing-howto\">version control logs online</ulink>. You can also checkout the code using SVN with the following call in the command line:"
msgstr ""

msgid "<computeroutput>$ </computeroutput><userinput>git clone https://salsa.debian.org/ddp-team/securing-debian-howto.git</userinput>"
msgstr "<computeroutput>$ </computeroutput><userinput>git clone https://salsa.debian.org/ddp-team/securing-debian-howto.git</userinput>"

msgid "Organizational notes/feedback"
msgstr "Notas/Retroalimentación/Organización"

msgid "Now to the official part. At the moment I (Alexander Reelsen) wrote most paragraphs of this manual, but in my opinion this should not stay the case. I grew up and live with free software, it is part of my everyday use and I guess yours, too. I encourage everybody to send me feedback, hints, additions or any other suggestions you might have."
msgstr "Ahora, la parte oficial. Tanto Alexander Reelsen como Javier Fernández-Sanguino escribieron la mayoría de párrafos de este manual, pero en opinión de ambos éste no debería ser el caso. Ambos han crecido y vivido con el software libre, es algo que usan a diario y supongo que usted también. Por eso animamos a todo el mundo a enviar todo tipo de retroalimentación, añadidos o cualquier otra sugerencia que usted pueda tener."

msgid "If you think, you can maintain a certain section or paragraph better, then write to the document maintainer and you are welcome to do it. Especially if you find a section marked as FIXME, that means the authors did not have the time yet or the needed knowledge about the topic. Drop them a mail immediately."
msgstr "Si desea mantener una cierta sección o mejor un párrafo, escriba a quien mantiene el documento y será bien recibido. Especialmente si encuentra una sección marcada como ARREGLAME, lo que significa que los autores no tienen el tiempo para hacerlo o el conocimiento total necesario sobre el tema, escríbales un correo inmediatamente."

msgid "The topic of this manual makes it quite clear that it is important to keep it up to date, and you can do your part. Please contribute."
msgstr "Por el tema de este manual está claro que es muy importante mantenerlo actualizado y usted puede hacer su parte. Por favor, contribuya."

msgid "Prior knowledge"
msgstr "Conocimiento previo"

msgid "The installation of Debian GNU/Linux is not very difficult and you should have been able to install it. If you already have some knowledge about Linux or other Unices and you are a bit familiar with basic security, it will be easier to understand this manual, as this document cannot explain every little detail of a feature (otherwise this would have been a book instead of a manual). If you are not that familiar, however, you might want to take a look at for where to find more in-depth information."
msgstr "La instalación de Debian GNU/Linux no es muy difícil y usted mismo debe haber sido capaz de instalarlo. Si tiene algún conocimiento sobre Linux u otro Unix y está familiarizado con la seguridad básica, le será más fácil entender este manual, dado que este documento no puede explicar cada pequeño detalle o característica (de lo contrario hubiera sido un libro en lugar de un manual). Si usted no está tan familiarizado, probablemente debería mirar <ulink url=\"references\" /> para saber como encontrar información más detallada."

msgid "Things that need to be written (FIXME/TODO)"
msgstr ""

msgid "This section describes all the things that need to be fixed in this manual. Some paragraphs include <emphasis>FIXME</emphasis> or <emphasis>TODO</emphasis> tags describing what content is missing (or what kind of work needs to be done). The purpose of this section is to describe all the things that could be included in the future in the manual, or enhancements that need to be done (or would be interesting to add)."
msgstr ""

msgid "If you feel you can provide help in contributing content fixing any element of this list (or the inline annotations), contact the main author (<xref linkend=\"authors\" />)."
msgstr ""

msgid "This document has yet to be updated based on the latest Debian releases. The default configuration of some packages need to be adapted as they have been modified since this document was written."
msgstr ""

msgid "Expand the incident response information, maybe add some ideas derived from Red Hat's Security Guide's <ulink url=\"http://www.redhat.com/docs/manuals/linux/RHL-9-Manual/security-guide/ch-response.html\">chapter on incident response</ulink>."
msgstr ""

msgid "Write about remote monitoring tools (to check for system availability) such as <application>monit</application>, <application>daemontools</application> and <application>mon</application>. See <ulink url=\"http://linux.oreillynet.com/pub/a/linux/2002/05/09/sysadminguide.html\">Sysamin Guide</ulink>."
msgstr ""

msgid "Consider writing a section on how to build Debian-based network appliances (with information such as the base system, <application>equivs</application> and FAI)."
msgstr ""

msgid "Check if <ulink url=\"http://www.giac.org/practical/gsec/Chris_Koutras_GSEC.pdf\">this site</ulink> has relevant info not yet covered here."
msgstr ""

msgid "Add information on how to set up a laptop with Debian, <ulink url=\"http://www.giac.org/practical/gcux/Stephanie_Thomas_GCUX.pdf\">look here</ulink>."
msgstr ""

msgid "Add information on how to set up a firewall using Debian GNU/Linux. The section regarding firewalling is oriented currently towards a single system (not protecting others...) also talk on how to test the setup."
msgstr ""

msgid "Add information on setting up a proxy firewall with Debian GNU/Linux stating specifically which packages provide proxy services (like <application>xfwp</application>, <application>ftp-proxy</application>, <application>redir</application>, <application>smtpd</application>, <application>dnrd</application>, <application>jftpgw</application>, <application>oops</application>, <application>pdnsd</application>, <application>perdition</application>, <application>transproxy</application>, <application>tsocks</application>). Should point to the manual for any other info. Note that <application>zorp</application> is now available as a Debian package and <emphasis>is</emphasis> a proxy firewall (they also provide Debian packages upstream)."
msgstr ""

msgid "Information on service configuration with file-rc."
msgstr ""

msgid "Check all the reference URLs and remove/fix those no longer available."
msgstr ""

msgid "Add information on available replacements (in Debian) for common servers which are useful for limited functionality. Examples:"
msgstr ""

msgid "local lpr with cups (package)?"
msgstr ""

msgid "remote lrp with lpr"
msgstr ""

msgid "bind with dnrd/maradns"
msgstr ""

msgid "apache with dhttpd/thttpd/wn (tux?)"
msgstr ""

msgid "exim/sendmail with ssmtpd/smtpd/postfix"
msgstr ""

msgid "squid with tinyproxy"
msgstr ""

msgid "ftpd with oftpd/vsftp"
msgstr ""

msgid "..."
msgstr ""

msgid "More information regarding security-related kernel patches in Debian, including the ones shown above and specific information on how to enable these patches in a Debian system."
msgstr ""

msgid "Linux Intrusion Detection (<application>kernel-patch-2.4-lids</application>)"
msgstr ""

msgid "Linux Trustees (in package <application>trustees</application>)"
msgstr "Confianza en Linux (dentro del paquete <application>trustees</application>)"

msgid "<ulink url=\"http://wiki.debian.org/SELinux\">NSA Enhanced Linux</ulink>"
msgstr ""

msgid "<application>linux-patch-openswan</application>"
msgstr "<application>ipac-ng</application>"

msgid "Details of turning off unnecessary network services (besides <command>inetd</command>), it is partly in the hardening procedure but could be broadened a bit."
msgstr ""

msgid "Information regarding password rotation which is closely related to policy."
msgstr ""

msgid "Policy, and educating users about policy."
msgstr ""

msgid "More about tcpwrappers, and wrappers in general?"
msgstr ""

msgid "<filename>hosts.equiv</filename> and other major security holes."
msgstr ""

msgid "Issues with file sharing servers such as Samba and NFS?"
msgstr ""

msgid "suidmanager/dpkg-statoverrides."
msgstr ""

msgid "lpr and lprng."
msgstr ""

msgid "Switching off the GNOME IP things."
msgstr ""

msgid "Talk about pam_chroot (see <ulink url=\"http://lists.debian.org/debian-security/2002/debian-security-200205/msg00011.html\" />) and its usefulness to limit users. Introduce information related to <ulink type=\"block\" url=\"http://online.securityfocus.com/infocus/1575\" />. <application>pdmenu</application>, for example is available in Debian (whereas flash is not)."
msgstr ""

msgid "Talk about chrooting services, some more info on <ulink url=\"http://www.linuxfocus.org/English/January2002/article225.shtml\">this Linux Focus article</ulink>."
msgstr ""

msgid "Talk about programs to make chroot jails. <application>compartment</application> and <application>chrootuid</application> are waiting in incoming. Some others (makejail, jailer) could also be introduced."
msgstr ""

msgid "More information regarding log analysis software (i.e. logcheck and logcolorise)."
msgstr ""

msgid "'advanced' routing (traffic policing is security related)."
msgstr ""

msgid "limiting <command>ssh</command> access to running certain commands."
msgstr ""

msgid "using dpkg-statoverride."
msgstr ""

msgid "secure ways to share a CD burner among users."
msgstr ""

msgid "secure ways of providing networked sound in addition to network display capabilities (so that X clients' sounds are played on the X server's sound hardware)."
msgstr ""

msgid "securing web browsers."
msgstr ""

msgid "setting up ftp over <command>ssh</command>."
msgstr ""

msgid "using crypto loopback file systems."
msgstr ""

msgid "ncrypting the entire file system."
msgstr ""

msgid "steganographic tools."
msgstr ""

msgid "setting up a PKA for an organization."
msgstr ""

msgid "using LDAP to manage users. There is a HOWTO of ldap+kerberos for Debian at <ulink url=\"http://www.bayour.com\" /> written by Turbo Fredrikson."
msgstr ""

msgid "How to remove information of reduced utility in production systems such as <filename>/usr/share/doc</filename>, <filename>/usr/share/man</filename> (yes, security by obscurity)."
msgstr ""

msgid "More information on lcap based on the packages README file (well, not there yet, see <ulink name=\"Bug #169465\" url=\"http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=169465\" />) and from the article from LWN: <ulink name=\"Kernel development\" url=\"http://lwn.net/1999/1202/kernel.php3\" />."
msgstr ""

msgid "Add Colin's article on how to setup a chroot environment for a full sid system (<ulink url=\"http://people.debian.org/~walters/chroot.html\" />)."
msgstr ""

msgid "Add information on running multiple <command>snort</command> sensors in a given system (check bug reports sent to <application>snort</application>)."
msgstr ""

msgid "Add information on setting up a honeypot (<application>honeyd</application>)."
msgstr ""

msgid "Describe situation wrt to FreeSwan (orphaned) and OpenSwan. VPN section needs to be rewritten."
msgstr ""

msgid "Add a specific section about databases, current installation defaults and how to secure access."
msgstr ""

msgid "Add a section about the usefulness of virtual servers (Xen et al)."
msgstr ""

msgid "Explain how to use some integrity checkers (AIDE, integrit or samhain). The basics are simple and could even explain some configuration improvements."
msgstr ""

msgid "Credits and thanks!"
msgstr ""

msgid "Alexander Reelsen wrote the original document."
msgstr "Alexander Reelsen escribió el documento original."

msgid "added more info to the original doc."
msgstr ""

msgid "Robert van der Meulen provided the quota paragraphs and many good ideas."
msgstr "Robert van der Meulen aportó los párrafos de quota y muchas buenas ideas."

msgid "Ethan Benson corrected the PAM paragraph and had some good ideas."
msgstr "Ethan Benson corrigió los párrafos de PAM y sugirió buenas ideas."

msgid "Dariusz Puchalak contributed some information to several chapters."
msgstr "Dariusz Puchalak hizo contribuciones a muchos capítulos."

msgid "Gaby Schilders contributed a nice Genius/Paranoia idea."
msgstr "Gaby Schilders contribuyó a una buena idea de Genio/Paranoia."

msgid "Era Eriksson smoothed out the language in a lot of places and contributed the checklist appendix."
msgstr "Era Eriksson resolvió problemas de idioma en muchos lugares y contribuyó al apéndice de la lista de comprobaciones."

msgid "Philipe Gaspar wrote the LKM information."
msgstr "Philipe Gaspar escribió la información de LKM."

msgid "Yotam Rubin contributed fixes for many typos as well as information regarding bind versions and MD5 passwords."
msgstr "Yotam Rubin contribuyó a los ajustes de muchos fallos ortográficos así como a la información con respecto a las versiones de bind y las contraseñas md5."

msgid "Francois Bayart provided the appendix describing how to set up a bridge firewall."
msgstr ""

msgid "Joey Hess wrote the section describing how Secure Apt works on the <ulink url=\"http://wiki.debian.org/SecureApt\">Debian Wiki</ulink>."
msgstr ""

msgid "Martin F. Krafft wrote some information on his blog regarding fingerprint verification which was also reused for the Secure Apt section."
msgstr ""

msgid "Francesco Poli did an extensive review of the manual and provided quite a lot of bug reports and typo fixes which improved and helped update the document."
msgstr ""

msgid "All the people who made suggestions for improvements that (eventually) were included here (see <xref linkend=\"guideformats\" />)."
msgstr ""

msgid "(Alexander) All the folks who encouraged me to write this HOWTO (which was later turned into a manual)."
msgstr "(de Alexander) A todas las personas que me animaron a escribir, este COMO (El cual posteriormente se convirtió en el manual) ."

msgid "The whole Debian project."
msgstr "La totalidad del proyecto Debian."

